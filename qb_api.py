"""
This module offer Quickbase API for python pandas Integration
"""
from copy import copy
import re
import io
import os
import json
import requests
from pathlib import Path
import urllib3
import lxml.etree as ET
import pandas as pd
from lib.util import check_environment_var, time_format, write_to_file
from lib.util import field_exsit_ckeck, dicttoxmlcreator, str_to_list, apply


_ENV_VARS = check_environment_var(['QBTOKEN', 'USERNAME', 'PASSWORD','https_proxy_server','http_proxy_server','CONFIG_FILE_PATH'])
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)  #disbale SSL warnings

if Path(_ENV_VARS['CONFIG_FILE_PATH']).exists():
    with Path(_ENV_VARS['CONFIG_FILE_PATH']).open() as f:
        _CONFIG_DICT = json.loads(f.read())
    if 'Production' in _CONFIG_DICT.keys():
        if _CONFIG_DICT['Production'] == "True":
            _PRODUCTION_DICT = check_environment_var(['QBTOKEN_PRODUCTION'])
            _ENV_VARS['QBTOKEN'] = _PRODUCTION_DICT['QBTOKEN_PRODUCTION']


_PARSER_DICTIONARY = {}

def clist_creator(list_or_pds):
    """
    :param list_or_pds: list or pandas series
    :return: clist string qb api need
    """
    return pd.Series(list_or_pds).astype('int').to_string(
        index=False, header=False).replace(
            '\n', '.').replace(
                ' ', '')

def query_creator(list_value:list,operand = 'EX',condition="OR",field_id='3'):
    list_value = apply(list_value,str)
    operand = str(operand)
    field_id = str(field_id)
    str_query = ''
    for value in list_value:
        str_query += '{'+field_id+'.'+operand+'.'+value+'}'+' '+condition+' '
    str_query = str_query[:-4]
    return str_query


def request_creator(
        qb_action,
        target_dbid,
        body={},
        app_token=None,
        target_domain,
        echo=True,
        proxies={
            'http': f"http://{_ENV_VARS['USERNAME']}:{_ENV_VARS['PASSWORD']}@{_ENV_VARS['http_proxy_server']}",
            'https': f"http://{_ENV_VARS['USERNAME']}:{_ENV_VARS['PASSWORD']}@{_ENV_VARS['https_proxy_server']}"},
        user_token=None,
        xml=None):
    '''
    :param qb_action: quickbase action
    :param target_dbid: target data base id
    :param body: request body (python dictionary)
    :param app_token: if we use user token, app_token is not needed.
    :param target_domain: quickbase realm
    :param echo: print respond status code
    :param proxies: set proxy if needed
    :param user_token: user token default it is should stored in environment variable
    :return: xml respond
    '''
    if not user_token:
        user_token = _ENV_VARS['QBTOKEN']
    # url
    url = f'https://{target_domain}/db/{target_dbid}'
    # header
    headers = {'Content-Type': 'application/xml'}
    headers.update({'QUICKBASE-ACTION': qb_action})
    # body (data)
    if xml:
        data = xml
    else:
        data = body
        data.update({'usertoken': user_token})
        if app_token:
            data.update({'apptoken': app_token})
        data = dicttoxmlcreator(
            data=data,
            custom_root='qdbapi',
            encoding='utf-8')
    _res = requests.post(url, proxies=proxies, data=data, headers=headers,verify = False)
    #write_to_file(data.decode(),'temp.xml','w') # for debug xml purpose
    if echo:
        print(f'{target_dbid} status code: ' + str(_res.status_code))
    return _res.text

def find_table_name(target_dbid):
    qb_action = 'API_GetSchema'
    xml = request_creator(
        target_dbid=target_dbid,
        qb_action=qb_action,
        body={})
    _return_data = qbxmlparser(qb_action=qb_action, xml=xml)
    table_name = _return_data['table']['table_name'].loc[0]
    return table_name


def qbxmlparser(qb_action, **kwargs):
    '''
    this function is to mapping the action to the right qb return xml body parser
    right now support: ['API_DoQuery','API_GetSchema','API_ImportFromCSV']
    :param qb_action: qb_action (string)
    :param **kwargs: kwargs goes into the parser we call
    :return: only dictionary
    '''
    # this function is to mapping the action to the right qb return xml body
    # parser
    return _PARSER_DICTIONARY[qb_action](**kwargs)


def get_dictionary_query(qbxmlparser_result, key, columns=None):
    """
    :param qbxmlparser_result: Pandas Dateframe
    :param key: key of dictionary
    :param columns: columns we need
    :return: Pandas Series
    """
    if columns:
        fields = qbxmlparser_result[key][columns]
    else:
        fields = qbxmlparser_result[key]
    return fields


def doquery_parser(**kwargs):
    '''
    :param kwargs: [xml]
    :return: pandas data frame
    '''
    xml = ET.fromstring(kwargs['xml'])
    # check successfully
    if xml.xpath('.//errcode')[0].text != str(0):
        print(
            f"error code: {xml.xpath('.//errcode')[0].text}  {xml.xpath('.//errtext')[0].text}")
        raise KeyError(
            f"error code: {xml.xpath('.//errcode')[0].text}  {xml.xpath('.//errtext')[0].text}")
    records = xml.xpath('.//record')

    if records:
        record = records[0]
        _col_need = apply(record.xpath('./*'), lambda x: x.tag)
        data = pd.DataFrame(data=apply(records, lambda x: apply(x.xpath('./*'), lambda x: x.text)), columns=_col_need)
    else:
        _col_need = []
        data = pd.DataFrame(data=[], columns=_col_need)
    return {'data': data}


_PARSER_DICTIONARY.update({'API_DoQuery': doquery_parser})


def getschema_parser(**kwargs):
    '''
    :param kwargs: [xml]
    :return: dictionary of three pandas dataframe ['table', 'query', 'field']
    '''
    xml = ET.fromstring(kwargs['xml'])
    tagswecarebesidestable = ['fields', 'queries', 'chdbids']
    _return_dict = {}
    # check successfully
    if xml.xpath('.//errcode')[0].text != str(0):
        print(
            f"error code: {xml.xpath('.//errcode')[0].text}  {xml.xpath('.//errtext')[0].text}")
        raise KeyError(
            f"error code: {xml.xpath('.//errcode')[0].text}  {xml.xpath('.//errtext')[0].text}")

    # table
    table = xml.xpath('.//table')[0]

    _col_need = ['table_name'] + apply(table.xpath('.//original/*'), lambda x: x.tag)

    _table_data = pd.DataFrame(columns=_col_need, data=[
        [table.xpath('./name')[0].text] + apply(table.xpath('.//original/*'), lambda x: x.text)])
    _return_dict.update({'table': _table_data})

    # other tags we care beside table
    tagswecarebesidestable = [tag for tag in xml.xpath(
        './table/*') if tag.tag in tagswecarebesidestable]
    for tagwecarebesidestable in tagswecarebesidestable:
        _col_need = []
        if tagwecarebesidestable.tag != 'chdbids':  # chdbids is a special case
            for value in tagwecarebesidestable:
                # get unique attribute of tag (e.g: query)
                _col_need = _col_need + [col for col in value.attrib.keys() if
                                     col not in _col_need and col != 'BR']
                # get unique tags
                _col_need = _col_need + [col for col in apply(value.xpath('./*'), lambda x: x.tag) if
                                     col not in _col_need and col != 'BR']
            _data = pd.DataFrame(columns=_col_need)
            for value in tagwecarebesidestable:
                _temp_data = pd.DataFrame(columns=value.attrib.keys() + apply(value.xpath('./*'), lambda x: x.tag),
                                        data=[value.attrib.values() + apply(value.xpath('./*'), lambda x: x.text)])
                _temp_data_2 = pd.DataFrame(columns=_col_need)
                _temp_data_2[_temp_data.columns] = _temp_data
                _data = pd.concat([_data, _temp_data_2], ignore_index=True)
        else:
            for value in tagwecarebesidestable:
                _col_need = _col_need + [col for col in value.attrib.keys() if
                                     col not in _col_need and col != 'BR']
                if value.tag not in _col_need:
                    _col_need = _col_need + [value.tag]
            _data = pd.DataFrame(columns=_col_need)
            for value in tagwecarebesidestable:
                _temp_data = pd.DataFrame(columns=value.attrib.keys(
                ) + [value.tag], data=[value.attrib.values() + [value.text]])
                # to solve dimenension discrepency
                _temp_data_2 = pd.DataFrame(columns=_col_need)
                _temp_data_2[_temp_data.columns] = _temp_data
                _data = pd.concat([_data, _temp_data_2], ignore_index=True)
        if not _data.empty:  # if it is not a empty data frame
            # remove completely empty data tag
            _data = _data.dropna(axis=1, how='all')
            _return_dict.update({tagwecarebesidestable.tag: _data})
    return _return_dict


_PARSER_DICTIONARY.update({'API_GetSchema': getschema_parser})


def import_fromcsv_parser(**kwargs):
    '''
    :param kwargs:['xml','table_name']
    :return: pandas data frame
    '''
    xml = ET.fromstring(kwargs['xml'])
    table_name = kwargs['table_name']
    # check successfully
    if xml.xpath('.//errcode')[0].text != str(0):
        print(
            f"error code: {xml.xpath('.//errcode')[0].text}  {xml.xpath('.//errtext')[0].text}")
        if len(xml.xpath('.//errdetail')) == 1:
            raise KeyError(f"error code: {xml.xpath('.//errcode')[0].text}  \
                    {xml.xpath('.//errtext')[0].text}\n{ET.tostring(xml.xpath('.//errdetail')[0]).decode('utf-8')}")
        else:
            raise KeyError(
                f"error code: {xml.xpath('.//errcode')[0].text}  {xml.xpath('.//errtext')[0].text}")
    tagwecare = [element for element in xml.xpath(
        './*') if element.tag != 'rids']
    data = pd.DataFrame(columns=['Date',
                                 'table_name'] + apply(copy(tagwecare), lambda x: x.tag),
                        data=[[time_format('')[1:-1],
                               table_name] + apply(copy(tagwecare), lambda x: x.text)])
    return {'import result': data}


_PARSER_DICTIONARY.update({'API_ImportFromCSV': import_fromcsv_parser})


def addrecord_parser(**kwargs):
    """
    parser of API_Addrecord
    :param kwargs:
    :return:
    """
    xml = ET.fromstring(kwargs['xml'])
    if xml.xpath('.//errcode')[0].text != str(0):
        print(
            f"error code: {xml.xpath('.//errcode')[0].text}  {xml.xpath('.//errtext')[0].text}")
        raise KeyError(
            f"error code: {xml.xpath('.//errcode')[0].text}  {xml.xpath('.//errtext')[0].text}")
    return xml.xpath('.//rid')[0].text


_PARSER_DICTIONARY.update({'API_AddRecord': addrecord_parser})


def API_API_EditRecord_parser(**kwargs):
    '''
    :param kwargs:['xml','table_name']
    :return: pandas data frame
    '''
    xml = ET.fromstring(kwargs['xml'])
    table_name = kwargs['table_name']
    # check successfully
    if xml.xpath('.//errcode')[0].text != str(0):
        print(
            f"error code: {xml.xpath('.//errcode')[0].text}  {xml.xpath('.//errtext')[0].text}")
        if len(xml.xpath('.//errdetail')) == 1:
            raise KeyError(f"error code: \
                    {xml.xpath('.//errcode')[0].text} \
                    {xml.xpath('.//errtext')[0].text}\n{ET.tostring( xml.xpath('.//errdetail')[0]).decode('utf-8')}")
        else:
            raise KeyError(
                f"error code: {xml.xpath('.//errcode')[0].text}  {xml.xpath('.//errtext')[0].text}")
    tagwecare = [element for element in xml.xpath(
        './*') if element.tag != 'rids']
    data = pd.DataFrame(columns=['Date',
                                 'table_name'] + apply(copy(tagwecare), lambda x: x.tag),
                        data=[[time_format('')[1:-1],
                               table_name] + apply(copy(tagwecare), lambda x: x.text)])
    return {'edit record result': data}


_PARSER_DICTIONARY.update({'API_EditRecord': API_API_EditRecord_parser})

def purgerecords_parser(**kwargs):
    '''
    :param kwargs: [xml]
    :return: pandas data frame
    '''
    xml = ET.fromstring(kwargs['xml'])
    # check successfully
    if xml.xpath('.//errcode')[0].text != str(0):
        print(
            f"error code: {xml.xpath('.//errcode')[0].text}  {xml.xpath('.//errtext')[0].text}")
        raise KeyError(
            f"error code: {xml.xpath('.//errcode')[0].text}  {xml.xpath('.//errtext')[0].text}")
    tagwecare = [element for element in xml.xpath(
            './*') if element.tag != 'rids']
    data = pd.DataFrame(columns=['Date',
                                 'table_name'] + apply(copy(tagwecare), lambda x: x.tag),
                        data=[[time_format('')[1:-1],
                               kwargs['table_name']] + apply(copy(tagwecare), lambda x: x.text)])
    return {'purge records result': data}


_PARSER_DICTIONARY.update({'API_PurgeRecords': purgerecords_parser})


def qb_fields_characterize_mapping(field_names, return_type='dictionary'):
    '''
    :param field_names: type: pandas series or list or single string
    :return: type:dictionary
    '''
    field_names = pd.Series(field_names)
    repd = re.compile(r'^\W')
    # if field label start with special character and qb will add '_' in the
    # beginning
    _field_names_key = field_names.str.replace(repd, '__')
    repd = re.compile(r'\W')
    _field_names_key = _field_names_key.str.lower().str.replace(
        repd, '_')  # qb replace special character with '_'
    if return_type == 'dictionary':
        return dict(zip(_field_names_key, field_names))
    if return_type == 'list':
        return list(_field_names_key)


def refresh_by_api_importfromcsv(
        pandas_data,
        target_dbid,
        app_name='source data',
        upload_filename='',
        raise_error=True):
    '''
    :param pandas_data: the pandas data frame you want to upload
    :param target_dbid: target dbid
    :param app_name: where the refresh data come from e.g: MRT, you can leave blank
    :param upload_filename: file name you make the pandas data,
    you can leave blank if you do not care which file has upload error
    :return: pandas dataframe of respond
    '''
    excel_data_file = pandas_data
    _s = io.StringIO()
    qb_action = 'API_GetSchema'
    xml = request_creator(
        target_dbid=target_dbid,
        qb_action=qb_action,
        body={})
    _return_data = qbxmlparser(qb_action=qb_action, xml=xml)
    table_name = _return_data['table']['table_name'].loc[0]
    fieldsinfo = get_dictionary_query(
        _return_data, key='fields', columns=[
            'id', 'label'])
    field_id_mapping = fieldsinfo[['label', 'id']].set_index('label')
    # columns exist check
    if raise_error:
        if field_exsit_ckeck(
                app_name,
                upload_filename,
                field_id_mapping.index,
                excel_data_file.columns,
                ".\\error.csv"):
            raise KeyError('fields are not matching')
    else:
        _error_string = field_exsit_ckeck(
            app_name,
            upload_filename,
            field_id_mapping.index,
            excel_data_file.columns,
            None,
            write_errormessage_to_file=False)
        if _error_string:
            return _error_string
    _id_columns_upload = field_id_mapping.loc[list(excel_data_file.columns)]
    excel_data_file.to_csv(_s, index=False)
    updatestring = _s.getvalue()
    body = {}
    body.update({'skipfirst': '1'})
    body.update({'records_csv': f'<![CDATA[{updatestring}]]>'})
    body.update({'clist': clist_creator(_id_columns_upload['id'])})
    # input parameter
    qb_action = 'API_ImportFromCSV'
    xml = request_creator(
        target_dbid=target_dbid,
        qb_action=qb_action,
        body=body)
    return qbxmlparser(
        qb_action=qb_action,
        xml=xml,
        table_name=table_name)['import result']


def add_record_by_api_addrecord(
        pandas_data,
        target_dbid,
        app_name='source data',
        upload_filename='',
        file_attachment_columns=None):
    '''
    :param pandas_data: pandas data you want to use API_AddRecord to add to quickbase
    :param target_dbid: target_dbid
    :param app_name: the name of the source application (e.g: MRT) ,
     you can leave blank
    :param upload_filename: where is the pandas data frame you get from
    (typically it is a excel file),
    you can leave blank if you do not care which file has upload error
    :param file_attachment_columns: the column name of the columns contains
     the file attachment dictionary
    The dictionary looks like {'fid': '8','file attchment':
    'base64 text but not binary text','filename': 'Change_Project_Pipeline.xlsx'}
    :return: a list of IDs of new added records
    '''
    file_attachment_columns = str_to_list(file_attachment_columns)
    _list_record_ids = []
    excel_data_file = pandas_data.reset_index(drop=True)
    qb_action = 'API_GetSchema'
    xml = request_creator(
        target_dbid=target_dbid,
        qb_action=qb_action,
        body={})
    _return_data = qbxmlparser(qb_action=qb_action, xml=xml)
    fieldsinfo = get_dictionary_query(
        _return_data, key='fields', columns=[
            'id', 'label'])
    field_id_mapping = fieldsinfo[['label', 'id']].set_index('label')
    # columns exist check
    if field_exsit_ckeck(
            app_name=app_name,
            upload_filename=upload_filename,
            qb_header=pd.Series(
                list(
                    qb_fields_characterize_mapping(
                        field_id_mapping.index).values())),
            upload_header=excel_data_file.columns,
            error_message_path=".\\error.csv"):
        raise KeyError('fields are not matching')
    qb_action = 'API_AddRecord'
    field_mapping = qb_fields_characterize_mapping(excel_data_file.columns)
    excel_data_file = excel_data_file.rename(columns=field_mapping)
    for index in excel_data_file.index:
        returndict = {}
        if file_attachment_columns:
            nonfileattachmentcolummns = excel_data_file.columns[~ excel_data_file.columns.isin(
                file_attachment_columns)]
        else:
            nonfileattachmentcolummns = excel_data_file.columns
        fileattachmentcolummns = excel_data_file.columns[excel_data_file.columns.isin(
            file_attachment_columns)]
        # though defind as list but the length of the list should be 0 or 1
        for col in nonfileattachmentcolummns:
            returndict.update({f'fielduniqueidentifier{col}':
                                   {'value': excel_data_file[col].loc[index], 'attr': {
                'name': f'\"{qb_fields_characterize_mapping(col, return_type="list")[0]}\"'}}})
        if len(fileattachmentcolummns) == len(file_attachment_columns):
            for col in file_attachment_columns:
                returndict.update({f'fielduniqueidentifier{col}': {
                    'value': excel_data_file[col].loc[index]['file attchment'],
                    'attr': {'fid': f'\"{excel_data_file[col].loc[index]["fid"]}\"',
                             'filename': f'\"{excel_data_file[col].loc[index]["filename"]}\"'}}})
        elif len(fileattachmentcolummns) > len(file_attachment_columns):
            raise KeyError(
                'There is more than one column has file attachment infomation, please check')

        returndict.update({'usertoken': _ENV_VARS['QBTOKEN']})
        xml = dicttoxmlcreator(returndict, custom_root='qdbapi')
        for col in sorted(
                list(
                    excel_data_file.columns),
                key=len,
                reverse=True):  # to advoid columns has inclusion sitiation
            xml = xml.replace(f'fielduniqueidentifier{col}', 'field')
        return_xml = request_creator(
            qb_action=qb_action, target_dbid=target_dbid, xml=xml)
        record_id = qbxmlparser(qb_action=qb_action, xml=return_xml)
        _list_record_ids.append(record_id)
    return _list_record_ids


def qb_api_get_data(
        target_dbid,
        characterize=False,
        need_columns=None,
        unix_time=False,
        time_zone='US/Eastern',
        query=None,
        qid=None,
        big_data=False,
        qb_xml_return_limit=20000000,
        additional_timeout_count=3,
        options=None,
        fill_na=None):
    '''
    :param target_dbid:target_dbid
    :param characterize:if you want to use quickbase default header,
    choose True (in most case, should be false)
    :param need_columns: if you want to specify which column you need
    :param query: query quickbase query string e.g: '{\'6\'.EX.\'Packiaraj Chidambaram\'}'
    :param qid: you can spefify the query report you want
    :return: pandas Data Frame
    '''
    if query and qid:
        raise ValueError(
            "you can only enter one input value among \'query\' and \'qid\' ")
    qb_action = 'API_GetSchema'
    xml = request_creator(
        target_dbid=target_dbid,
        qb_action=qb_action,
        body={})
    _return_data = qbxmlparser(qb_action=qb_action, xml=xml)
    fields = get_dictionary_query(
        _return_data, key='fields', columns=[
            'id', 'field_type', 'label'])
    if need_columns:
        fields = fields[fields['label'].isin(need_columns)]
    qb_action = 'API_DoQuery'
    body = {'clist': clist_creator(fields['id'])}
    if query:
        body.update({'query': query})
    if qid:
        body.update({'qid': qid})
    if options:
        body.update({'options': options})
    xml = request_creator(
        target_dbid=target_dbid,
        qb_action=qb_action,
        body=body)
    try:
        data = qbxmlparser(qb_action=qb_action, xml=xml)['data']
        if data.empty: # if the data frame is empty, we directly return a empty df with columns
            return pd.DataFrame(columns=fields['label'])
    except Exception as e:
        if not big_data:
            raise e
        elif e.args[0] == 'error code: 75  Report too large':
            print('since the report is too large, we will query in loop')
            xml = request_creator(
                target_dbid=target_dbid,
                qb_action=qb_action,
                body=body)
            loop_step = 100
            loop_step_unit = 1
            loop_query = '{' + \
                '3.LT.{}'.format(loop_step * loop_step_unit) + '}'
            if query:
                loop_query = query + 'AND' + loop_query
                body.update({'query': query})
            else:
                body.update({'query': loop_query})
                xml = request_creator(
                    target_dbid=target_dbid, qb_action=qb_action, body=body)
                data = qbxmlparser(qb_action=qb_action, xml=xml)['data']
                loop_step_unit = round(
                    qb_xml_return_limit / (len(xml) * loop_step / data.shape[0])) - 1
                int_pointer = loop_step
                while additional_timeout_count > 0:
                    loop_query = '{' + '3.GTE.{}'.format(int_pointer) + '}' +\
                                 'AND' + '{' + '3.LT.{}'.format(
                                     int_pointer + loop_step * loop_step_unit) + '}'
                    # update pointer
                    int_pointer = int_pointer + loop_step * loop_step_unit
                    if query:
                        loop_query = query + 'AND' + loop_query
                        body.update({'query': query})
                    else:
                        body.update({'query': loop_query})
                    xml = request_creator(
                        target_dbid=target_dbid, qb_action=qb_action, body=body)
                    df_additional_data = qbxmlparser(
                        qb_action=qb_action, xml=xml)['data']
                    if df_additional_data.shape[0] == 0:
                        additional_timeout_count = additional_timeout_count - 1
                    data = pd.concat([data, df_additional_data], axis=0)
    if not unix_time:
        date_field_list = fields['label'][fields['field_type'] == 'date']
        timestamp_field_list = fields['label'][fields['field_type']
                                             == 'timestamp']
        duration_field_list = fields['label'][fields['field_type'] == 'duration']
        for col in date_field_list:
            col = list(qb_fields_characterize_mapping(col).keys())[0]
            data[col] = pd.to_datetime(data[col], unit='ms', errors='ignore')
        for col in duration_field_list:
            col = list(qb_fields_characterize_mapping(col).keys())[0]
            data[col] = pd.to_timedelta(data[col], errors='ignore') * (
                10 ** 6)  # ns pandas default, ms is quickbase default
        for col in timestamp_field_list:
            col = list(qb_fields_characterize_mapping(col).keys())[0]
            data[col] = pd.to_datetime(
                data[col],
                unit='ms',
                errors='ignore').dt.tz_localize('UTC').dt.tz_convert(time_zone)
            data[col] = data[col].astype('str').str[:-6]
            data[col] = pd.to_datetime(data[col], errors='ignore')
    if need_columns:
        try:
            data = data.drop(columns='update_id')
        except BaseException:
            pass
    if not characterize:
        data = data.rename(
            columns=qb_fields_characterize_mapping(
                fields['label']))  # rename columns
    if need_columns:
        data = data[need_columns]  # order the columns
    if fill_na:
        data = data.fill_na(fill_na)
    return data


def edit_record_by_api_editrecord(
        pandas_data,
        target_dbid,
        table_name,
        app_name='source data',
        key_field='Record ID',
        upload_filename='',
        file_attachment_columns=None):
    """
    :param pandas_data: pandas dataframe
    :param target_dbid: target database id
    :param table_name:  name of the table you want to edit
    :param app_name: name of the app you want to edit
    :param key_field: key field
    :param upload_filename: name of the file you want to upload
    :param file_attachment_columns: name of the column has the attachment
    :return:
    """
    file_attachment_columns = str_to_list(file_attachment_columns)
    _record_collector = pd.DataFrame()
    excel_data_file = pandas_data.reset_index(drop=True)
    qb_action = 'API_GetSchema'
    xml = request_creator(
        target_dbid=target_dbid,
        qb_action=qb_action,
        body={})
    _return_data = qbxmlparser(qb_action=qb_action, xml=xml)
    fieldsinfo = get_dictionary_query(
        _return_data, key='fields', columns=[
            'id', 'label'])
    field_id_mapping = fieldsinfo[['label', 'id']].set_index('label')
    # columns exist check
    if field_exsit_ckeck(
            app_name=app_name,
            upload_filename=upload_filename,
            qb_header=pd.Series(
                list(
                    qb_fields_characterize_mapping(
                        field_id_mapping.index).values())),
            upload_header=excel_data_file.columns,
            error_message_path=".\\error.csv"):
        raise KeyError('fields are not matching')
    qb_action = 'API_EditRecord'
    field_mapping = qb_fields_characterize_mapping(excel_data_file.columns)
    excel_data_file = excel_data_file.rename(columns=field_mapping)

    for index in excel_data_file.index:
        returndict = {}
        nonfileattachmentcolummns = excel_data_file.columns[
            ~excel_data_file.columns.isin(file_attachment_columns + [key_field])]
        fileattachmentcolummns = excel_data_file.columns[excel_data_file.columns.isin(
            file_attachment_columns)]
        # though defind as list but the length of the list should be 0 or 1
        for col in nonfileattachmentcolummns:
            returndict.update({f'fielduniqueidentifier{col}':
                                   {'value': excel_data_file[col].loc[index], 'attr': {
                'name': f'\"{qb_fields_characterize_mapping(col, return_type="list")[0]}\"'}}})
        if len(fileattachmentcolummns) == len(file_attachment_columns):
            for col in file_attachment_columns:
                returndict.update({f'fielduniqueidentifier{col}': {
                    'value': excel_data_file[col].loc[index]['file attchment'],
                    'attr': {'fid': f'\"{excel_data_file[col].loc[index]["fid"]}\"',
                             'filename': f'\"{excel_data_file[col].loc[index]["filename"]}\"'}}})
        elif len(fileattachmentcolummns) > len(file_attachment_columns):
            raise KeyError(
                'There is more than one column has file attachment infomation, please check')
        returndict.update({'key': excel_data_file[key_field].loc[index]})
        returndict.update({'usertoken': _ENV_VARS['QBTOKEN']})
        xml = dicttoxmlcreator(returndict, custom_root='qdbapi')
        for col in sorted(
                list(
                    excel_data_file.columns),
                key=len,
                reverse=True):  # to advoid columns has inclusion sitiation
            xml = xml.replace(f'fielduniqueidentifier{col}', 'field')
        return_xml = request_creator(
            qb_action=qb_action, target_dbid=target_dbid, xml=xml)
        record = qbxmlparser(
            qb_action=qb_action,
            xml=return_xml,
            table_name=table_name)
        _record_collector = pd.concat(
            [_record_collector, record['edit record result']])
    return _record_collector


def qb_api_purge_records(target_dbid, record_id=None, qid=None, query=None, qname=None, clear_all=False):
    qb_action = "API_PurgeRecords"
    int_parameter_value_count = 0
    for value in [record_id, qid, query, qname, clear_all]:
        if value:
            int_parameter_value_count += 1
    if int_parameter_value_count != 1:
        raise KeyError("if and if only one conditional should be in the input, purge process can run")
    body = {}
    if record_id:
        body.update({"query": query_creator(record_id, operand='EX', condition="OR", field_id='3')})
    if qid:
        body.update({"qid": qid})
    if query:
        body.update({"query": query})
    if qname:
        body.update({"qname": qname})
    app_token = None
    target_domain = '??'
    echo = True
    proxies = {
        'http': f"http://{_ENV_VARS['USERNAME']}:{_ENV_VARS['PASSWORD']}@{_ENV_VARS['http_proxy_server']}",
        'https': f"http://{_ENV_VARS['USERNAME']}:{_ENV_VARS['PASSWORD']}@{_ENV_VARS['https_proxy_server']}"}
    user_token = None
    xml = None
    if not user_token:
        user_token = _ENV_VARS['QBTOKEN']
    # url
    url = f'https://{target_domain}/db/{target_dbid}'
    # header
    headers = {'Content-Type': 'application/xml'}
    headers.update({'QUICKBASE-ACTION': qb_action})
    # body (data)
    if xml:
        data = xml
    else:
        data = body
        data.update({'usertoken': user_token})
        if app_token:
            data.update({'apptoken': app_token})
        data = dicttoxmlcreator(
            data=data,
            custom_root='qdbapi',
            encoding='utf-8')
    _res = requests.post(url, proxies=proxies, data=data, headers=headers, verify=False)

    return qbxmlparser(qb_action=qb_action, table_name=find_table_name(target_dbid), xml=_res.text)[
        'purge records result']