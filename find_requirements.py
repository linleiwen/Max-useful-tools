import pip
import sys
import re
from lib.util import apply
import pandas as pd
import os
from pathlib import Path
import distutils.sysconfig as sysconfig
import os
ignore_package = ['lib','cme','pip']
ignore_dir = ['env']

df_global_package = pd.DataFrame(data=list(map(lambda x:str(x).split(' '),pip.get_installed_distributions(local_only=True))),columns=['package_name','version'])
std_lib = sysconfig.get_python_lib(standard_lib=True)
list_all_stardard_lib = []
for top, dirs, files in os.walk(std_lib):
    #print(dirs)
    if top.find('site-packages')==-1:
        for nm in files:
            if nm != '__init__.py' and nm[-3:] == '.py':
                #print(os.path.join(top, nm)[len(std_lib)+1:-3].replace(os.sep, '.'))
                list_all_stardard_lib.append(nm[:-3])
            elif nm == '__init__.py':
                list_all_stardard_lib.append(top[len(std_lib)+1:])
list_all_stardard_lib += dir(__builtins__)+list(sys.builtin_module_names)

def find_files_in_dir_recurrsively(re_pattern=".*.py\Z",path='.',path_lib=True):
    list_files = []
    path = Path(path)
    for item in os.listdir(path):
        path_item = path/item
        if os.path.isdir(path_item):
            if item not in ignore_dir:
                list_files.extend(find_files_in_dir_recurrsively(path=path/Path(item),re_pattern=re_pattern,path_lib=True))
        elif os.path.isfile(path_item):
            if re.search(re_pattern,str(path_item)):
                list_files.append(path_item)
    if not path_lib:
        list_files = apply(list_files,str)
    return list_files

# for top, dirs, files in os.walk(std_lib):


def extract_import(python_script):
    list_import = []
    list_lines_import = list(filter(lambda x: x.find('import') != -1, python_script.split('\n')))

    global list_all_stardard_lib
    for line in list_lines_import:
        # extract 'from'
        if line.startswith('from '):
            package_name = re.search('from \s*(?P<package_name>\w*)\s*', line).group('package_name')
            if package_name not in list_all_stardard_lib:
                list_import.append(package_name)
        # extract 'import'
        elif line.startswith('import '):
            comment_index = line.find('#')
            if comment_index != -1:
                line = line[:comment_index]
            list_temp = apply(line.replace('import', '').replace('#.*', '').split(','),
                              lambda x: x.split(' as ')[0].strip().split('.')[0])
            list_temp = [item for item in list_temp if item not in list_all_stardard_lib]
            list_import.extend(list_temp)
    return list(set(list_import))

def find_used_modules_in_project():
    list_modules_in_project = []
    for file in find_files_in_dir_recurrsively():
        with file.open() as f:
            list_modules_in_project+=extract_import(f.read())
    return list(set(list_modules_in_project))



df_global_package = pd.DataFrame(data=list(map(lambda x:str(x).split(' '),pip.get_installed_distributions(local_only=True))),columns=['package_name','version'])
list_package_in_project = []
for file in find_files_in_dir_recurrsively():
    with file.open() as f:
        list_package_in_project+=extract_import(f.read())
list_package_in_project = list(filter(lambda x:x not in ignore_package,list_package_in_project))
try:
    list_package_in_project = list(set(list_package_in_project))
    list_package_in_project.remove('')
except:
    pass

df_global_package['module_name'] = df_global_package['package_name'].str.lower().str.replace('-','_')
df_dir_package = pd.DataFrame(data = list_package_in_project,columns = ['module_name'])
mask_found = df_dir_package['module_name'].isin(df_global_package['module_name'])
df_found = df_global_package[df_global_package['module_name'].isin(list(df_dir_package['module_name'][mask_found]))]
df_not_found = df_dir_package[~mask_found]
str_to_requirements_txt = ''
for index, row in df_found.iterrows():
    str_to_requirements_txt += row['package_name']+'=='+row['version']+'\n'
for index, row in df_not_found.iterrows():
    str_to_requirements_txt += row['module_name']+'=='+'??????'+'\n'

print(str_to_requirements_txt)