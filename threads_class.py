from threading import Thread
from lib.maxselfdef import run_script_in_python
import time
import uuid

class Multi_Thread_Python_Script(Thread):
    def __init__(self, target_function, wait_interval=1, time_out=3600, **function_kwargs):
        Thread.__init__(self)
        self.target_function = target_function
        self.function_kwargs = function_kwargs
        self.bool_end_flag = 'end_flag'+str(uuid.uuid1())
        self.result = self.bool_end_flag
        self.wait_interval = wait_interval
        self.time_out = time_out
    def run(self):
        self.result = self.target_function(**self.function_kwargs)
    def result_collector(self):
        count = 0
        max_wait_count = self.time_out/self.wait_interval
        while self.result == self.bool_end_flag:
            time.sleep(self.wait_interval)
            count += 1
            if count>max_wait_count:
                raise TimeoutError('Exceed the allowed excuting time')
        return self.result
