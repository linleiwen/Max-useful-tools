''' author: Max Lin
Your code has been rated at 9.02/10 in pylint
'''

import time
from datetime import datetime, timedelta
import os
import calendar
import pandas as pd
from pathlib import Path


# def js_wait(driver):
#     """
#     This function is for Appian system loading wait
#     :param driver: selenium driver object
#     :return: None
#     """
#     _element = driver.find_element_by_xpath(
#         "//div[@class='appian-indicator-message']")
#     while _element.get_attribute("aria-hidden") != 'true':
#         time.sleep(0.5)
#         _element = driver.find_element_by_xpath(
#             "//div[@class='appian-indicator-message']")
#     time.sleep(1)


def digit_adj(number, digit=2):
    '''
    This function is to add left leading zero to the number and return as string
    :param number: number or number of string
    :param digit: how many the digit of the return string
    :return: string
    '''
    number = str(number)
    if digit > len(number):
        number = "0" * (digit - len(number)) + number
    return number


def time_format(filetype='xlsx', minute_sensitive=True,
                input_time_stamp='now'):
    '''
    :param filetype: extension of the file
    :param minute_sensitive: minute specific?
    :param input_time_stamp: 'now' or e.g:{'time':'2019/2/22','format':'%Y/%m/%d'}
    :return: return a time stamp postfix. Eg: _2018_10_01_09_34 AM.xlsx
    '''
    if input_time_stamp == 'now':
        time_obj = datetime.now()
    else:
        time_obj = datetime.strptime(
            input_time_stamp['time'],
            input_time_stamp['format'])
    if minute_sensitive:
        if time_obj.hour > 11:
            if time_obj.hour == 12:
                return f"_{time_obj.year}_{digit_adj(time_obj.month)}" \
                    f"_{digit_adj(time_obj.day)}" \
                    f"_12_{digit_adj(time_obj.minute)} PM.{filetype}"
            else:
                return f"_{time_obj.year}_{digit_adj(time_obj.month)}" \
                    f"_{digit_adj(time_obj.day)}_" \
                    f"{digit_adj(time_obj.hour%12)}" \
                    f"_{digit_adj(time_obj.minute)} PM.{filetype}"
        else:
            return f"_{time_obj.year}_{digit_adj(time_obj.month)}" \
                f"_{digit_adj(time_obj.day)}" \
                f"_{digit_adj(time_obj.hour)}_{digit_adj(time_obj.minute)} AM.{filetype}"
    else:
        return f"_{time_obj.year}_{digit_adj(time_obj.month)}" \
            f"_{digit_adj(time_obj.day)}.{filetype}"


def filename_timestamper(file_path, minute_sensitive=False):
    '''add time stamp to a file'''
    import re
    pattern = r'(?P<fullPath>.*)\.(?P<extensionType>.*)'
    restring = re.search(pattern, str(Path(file_path)))

    _full_path = restring.group('fullPath')
    _extension_type = restring.group('extensionType')

    return Path(_full_path + \
        time_format("", minute_sensitive=minute_sensitive) + _extension_type)


def change_NO_to_hyperlink(text):
    '''change to text string as a excel internal hyperlink'''
    return f'=HYPERLINK("#{text}!A1","{text}")'


def apply(array, function):
    '''this is apply function for list'''
    return list(map(function, array))


def extract_text(react_text):
    '''this function is able to return text or react text from soup object.
     If it is none, return a space '''
    import re
    pattern = r'-->(?P<col>.*)<!--'
    if re.search(pattern, str(react_text)) is not None:
        return re.search(pattern, str(react_text)).group('col')
    elif react_text.text is not None:
        return react_text.text
    else:
        return " "


def check_environment_var(need_environment_var_list=["chromedriver"]):
    '''This function return a environment variable dictionary'''
    environment_var_dict = {}
    for var_key in need_environment_var_list:
        try:
            environment_var_dict[var_key] = os.environ[var_key]
        except BaseException:
            print(
                f"Please add environment variable for {var_key}, and run the APP")
            time.sleep(10)
            raise AttributeError
    return environment_var_dict


def crop(image_path, coords, saved_location):
    """
    @param image_path: The path to the image to edit
    @param coords: A tuple of x/y coordinates (x1, y1, x2, y2)
    @param saved_location: Path to save the cropped image
    """
    from PIL import Image
    image_obj = Image.open(image_path)
    cropped_image = image_obj.crop(coords)
    cropped_image.save(saved_location)

def list_spliter(list_obj, chuck_size=4):
    """
    separate list into smaller chunck size
    :param list_obj: list
    :param chuck_size: chunk size
    :return: list
    """
    return [list_obj[i:i + chuck_size]
            for i in range(0, len(list_obj), chuck_size)]


def copy_folder(src, dst):
    """
    Copy pastes file from src to dst
    :param src: source path
    :param dst: distination path
    :return: None
    """
    from shutil import copyfile
    import shutil
    if os.path.isfile(src):
        try:
            copyfile(src, dst)
        except BaseException:
            print(f'{src} failed!')

    if os.path.isdir(src):
        if os.path.isdir(dst):
            shutil.rmtree(dst)
        os.makedirs(dst)
        files = os.listdir(src)
        for file in files:
            copy_folder(src=Path(f'{src}/{file}'), dst=Path(f'{dst}/{file}'))


def check_folder_exist(folder_name):
    """
    :param folder_name: the folder(directorary we are going to check whether exist)
    :return: None (if it does not exist, the def will create one automatically)
    """
    if not os.path.isdir(folder_name):
        os.mkdir(folder_name)

def find_latest_file(folders, file_name_contains, download_file=True):
    '''
    :param folders: the folders we are going to search
    :param file_name_contains: part of string file name contains,
    e.g: filename.extension or filename (#).extension
    :return: the full route of the latest file
    '''
    import re
    if download_file:
        pattern = rf'(^{file_name_contains} \([0-9]*\)\..*)|(^{file_name_contains}\..*)'
    else:
        pattern = f'(^{file_name_contains}.*)'
    if isinstance(folders, Path):
        folders = [folders]
    folders = apply(folders, Path) # all element in list is Path Object
    latest_folder = folders[0]
    # if file is not in the first folder then remove the folder and assign to
    # next folder until find it
    while len([file for file in os.listdir(str(latest_folder)) if re.search(
            pattern, file)]) == 0:
        folders.pop(0)
        if len(folders) == 0:
            raise FileNotFoundError(
                f'We can not find file: {file_name_contains}')
        latest_folder = folders[0]
    # define the first folder and first file in that folder as latest file
    latest = [file for file in os.listdir(
        latest_folder) if re.search(pattern, file)][0]
    for folder in folders:
        files = [file for file in os.listdir(
            folder) if re.search(pattern, file)]
        for file in files:
            if os.path.getctime(
                    Path(folder)/Path(file)) > os.path.getctime(Path(latest_folder)/Path(latest)):
                latest = file
                latest_folder = folder
    return Path(latest_folder)/Path(latest)

def copypaste_latest_file(download_route, dest_route,
                          file_name_contains, postfix=".xlsx"):
    '''
    :param download_route: Download route
    :param dest_route: Destination route
    :param file_name_contains: the part string of downloaded file name
    :param postfix: extension type profix (".xlsx" is default)
    :return: None (copy paste the lastest download file form download to target folder)
    '''
    from shutil import copyfile
    copyfile(
        find_latest_file(
            download_route,
            file_name_contains),
        Path(
        dest_route +
        '/' +
        file_name_contains +
        postfix)
    )

def similar_compare(str_x, str_y, comprise=True, threshold=0.88):
    '''
    :param str_x: string
    :param str_y: string
    :param comprise:  str_x comprise str_y or str_x comprise str_x will return True
    :param threshold: similarity percentage threshold
    :return: Boolean
    '''
    from difflib import SequenceMatcher

    def similar(str_a, str_b):
        """
        :param str_a: string
        :param str_b: string
        :return: Boollan
        """
        return SequenceMatcher(None, str_a, str_b).ratio() > threshold

    flag = (((str_x in str_y) or (str_y in str_x)) and comprise) or similar(str_x, str_y)
    return flag


def string_in_pds(string, pds):
    """
    check string is in the pd Series
    :param string:
    :param pds:
    :return:
    """
    return pds.apply(lambda x: string in x)


def similar_mapping_key_dictionary(pds_x, pds_y, comprise=False,
                                   threshold=0.98):
    '''
    :param pds_x: pd.Series x
    :param pds_y: pd.Series y
    :param comprise:  pds_x comprise y or pds_x comprise pds_x will think they are matching key
    :param threshold: similarity percentage threshold
    :return: dictionary of mapping key like keyDictionary[x] = y
    Note y must contain all items of x
    '''
    from difflib import SequenceMatcher
    pds_x = pd.Series(pds_x.unique()).copy()
    pds_y = pd.Series(pds_y.unique()).copy()
    pds_x.sort_values(inplace=True)
    pds_y.sort_values(inplace=True)
    dictionary = {}
    for i in range(pds_x.shape[0]):
        str_x = pds_x.iloc[i]
        temp = []
        for j in range(pds_y.shape[0]):
            str_y = pds_y.iloc[j]
            similar_score = SequenceMatcher(None, str_x, str_y).ratio()
            temp.append(similar_score)
            # the str will find the best match y string
            if (((str_x in str_y) or (str_y in str_x)) and comprise):
                dictionary[pds_x.iloc[i]] = pds_y.iloc[j]
            elif similar_score > threshold and similar_score == max(temp):
                dictionary[pds_x.iloc[i]] = pds_y.iloc[j]
    return dictionary


def to_universe_date_format(
        pds, errors='coerce', to_string=True, NaT_replace=' '):
    '''
    :param pds: pandas Series (str or object type)
    :param errors: if we encounter errors, what should we do
    :param to_string:  Return str or pd time series
    :param NaT_replace: if we choose str, what should we replace NaT
    :return: pd Series with universal data format
    '''
    pds = pd.to_datetime(pds, errors)
    if to_string:
        pds = pds.astype('str')
        pds = pds.str.replace('NaT', NaT_replace)
    return pds


def to_traditional_date_format_string(string):
    """
    transfrom date format to traditional date format
    e.g: 2019-07-02 -> 07/02/2019
    :param string:
    :return: string
    """
    import re
    research_result = re.search(
        r'(?P<year>\d{4}?)-(?P<month>\d{2}?)-(?P<day>\d{2}?)(?P<other>.*)', string)
    if research_result is None:
        return ' '
    else:
        return research_result.group('month') + '/' + research_result.group(
            'day') + '/' + research_result.group('year') + research_result.group('other')


def to_traditional_date_format(pds, NaT_replace=' '):
    """
    transfrom date format to traditional date format
    e.g: 2019-07-02 -> 07/02/2019
    :param: pd.Series
    :return: string
    """
    pds = pds.astype('str')
    return pds.apply(to_traditional_date_format_string)


def linefeed_remove_strip(pds):
    '''
    replace '\n' and apply strip() on a string pds
    '''
    return pds.str.replace('\n', ' ').str.strip()


def getwebdriver(url="", hidden=False, initial_waittime=3):
    """
    # Start the chrome driver with selenium
    :param url: starting url
    :param hidden: hidden the browser??
    :param initial_waittime: loading starting url after waiting a period of time
    :return: selenium driver
    """
    # Start the chrome driver with selenium
    # https://stackoverflow.com/questions/43079018/selenium-chromedriver-failed-to-load-extension
    from selenium.webdriver.chrome.options import Options
    from selenium import webdriver

    environment_var_dict = check_environment_var(["chromedriver"])
    options = Options()
    options.add_argument("--start-maximized")
    options.add_argument("--disable-automation")
    options.add_experimental_option("useAutomationExtension", False)
    if hidden:
        options.add_argument('--headless')
    driver = webdriver.Chrome(
        environment_var_dict["chromedriver"],
        options=options)
    time.sleep(initial_waittime)
    if url != "":
        driver.get(url)
    return driver


def disjoin_dataframe(input_df, col_name_df1, col_name_df2, keys):
    '''
    :param input_df: input DataFrame (the DataFrame we want to disjoin)
    :param col_name_df1: columns of output disjoined DataFrame
    (this DataFrame contains information of entities)
    :param col_name_df2: columns of output disjoined DataFrame
    (this DataFrame contains records of each entity)
    :param keys: disjoin (or join) keys
    :return: output disjoined DataFrame (this DataFrame contains information of entities),
     output disjoined DataFrame (this DataFrame contains records of each entity)
    '''
    df1_return = input_df[col_name_df1].drop_duplicates()
    df1_return.reset_index(drop=True, inplace=True)
    df2_return = input_df[col_name_df2]
    if df2_return.shape[0] != df2_return.drop_duplicates().shape[0]:
        print('Warning: The dataframe still have duplicate records. Please check!')
    df2_return.reset_index(drop=True, inplace=True)
    rejoin = pd.merge(df1_return, df2_return, on=keys)
    # dimension check (demensions of rejoin dataframe and input dataframe
    # should be consistent)
    if ((rejoin.columns.sort_values() == input_df.columns.sort_values()).sum()
            == input_df.shape[1]) & (rejoin.shape[0] == input_df.shape[0]):
        print('Bingo: Disjoin data frames pass dimension check')
    else:
        raise Warning('disjoin data frames do not pass dimension check！！！')
    return df1_return, df2_return


def npfoalt_to_integer_string(pds, replacenan=''):
    '''
    this function transform pandas from float to integet string
    :param pds: pandas series (type:float)
    :param replacenan: the string to replace nan
    :return: pandas series(type:str)
    '''
    import re
    _re_pd = re.compile(r'\..*')
    pds = pds.astype('str')
    pds = pds.replace('nan', replacenan)
    return pds.replace(_re_pd, '')


def slacktoggle(string, origin='/', replace='\\'):
    """
    :param string: input string
    :param origin: to replace
    :param replace:  replace with
    :return: string
    """
    return string.replace(origin, replace)


def match_check(app_name, upload_filename, qb_header,
                upload_header, error_message_path):
    """
    :param app_name: string
    :param upload_filename: string
    :param qb_header: list or pd.Series
    :param upload_header: list or pd.Series
    :param error_message_path: string(path)
    :return: boolean (match return false, does not math return true)
    """
    list1 = pd.Series(qb_header)
    list2 = pd.Series(upload_header)
    mask = ~(list1 == list2)
    error_string = ""
    count = 0
    for index in list1[mask].index:
        count += 1
        strerr = f"{upload_filename}, Names Mismatch: {count}, " \
            f"QuickBase: {list1[index]}, {app_name}: {list2[index]}"
        print(strerr)
        error_string = error_string + strerr + "\n"
    flag_fix = (error_string != "")
    if flag_fix:
        time.sleep(5)
        f = open(error_message_path, "a")
        f.write(error_string)
        f.close()
        print("See details in errors.txt")
    return flag_fix


def field_exsit_ckeck(app_name, upload_filename, qb_header, upload_header,
                      error_message_path, write_errormessage_to_file=True):
    # for auickbase api upload check use.
    # check fields of upload file are in the quick base schema
    '''
    :param app_name: where the upload file come from
    :param upload_filename: name of the upload file come from
    :param qb_header: field names of the quickbase table
    :param upload_header: field names of the upload file
    :param error_message_path: if there is a error,
    specify the path you want to write the error message
    :param write_errormessage_to_file: if it is True,
    function return a boolean, 'True' means mismatch;
    if it is False, function return error message or None value
    :return: boolean or string
    '''
    error_string = ""
    mask1 = ~upload_header.isin(qb_header)
    mask2 = ~qb_header.isin(upload_header)
    discrepancy_dict = similar_mapping_key_dictionary(
        upload_header[mask1], qb_header[mask2], comprise=True, threshold=0.7)
    for _upload_field in upload_header[mask1]:
        strerr = f"In {upload_filename}, we cannot find {app_name} field: {_upload_field}"
        if _upload_field in discrepancy_dict.keys():
            strerr = strerr + \
                f', but it potential match QuickBase:{discrepancy_dict[_upload_field]}'
        print(strerr)
        error_string = error_string + strerr + "\n"
    flag_fix = (error_string != "")
    if write_errormessage_to_file:
        if flag_fix:
            f = open(error_message_path, "a")
            f.write(error_string)
            f.close()
            print("See details in errors.txt")
        return flag_fix
    else:
        if flag_fix:
            return error_string
        else:
            return None


class TimerCls:
    '''
    output_type: return data type: ['timedelta':timedelta object,
    'total_second': floating poinr in terms of minute]
    timer = TimerCls()
    timer.start()
    timer.end()
    '''

    def __init__(self, output_type='timedelta'):
        self.output_type = output_type

    def start(self):
        """ counting start """
        self.starttime = datetime.now()

    def end(self):
        """ counting end """
        if self.starttime:
            self.endtime = datetime.now()
            switch = {}
            switch.update({'timedelta': self.endtime - self.starttime})
            switch.update(
                {'total_second': (self.endtime - self.starttime).total_seconds()})
            return switch[self.output_type]


def write_to_file(string_content, file_path='temp.html',
                  mode='w', encoding='utf-8'):
    '''
    :param string_content: string content
    :param file_path: file path to write
    :param mode: mode
    :param encoding: encoding
    :return: none
    '''
    with open(file_path, mode=mode, encoding=encoding) as f:
        f.write(string_content)


def dicttoxmlcreator(data, custom_root=None, encoding=None):
    '''
    :param data: python dictionary e.g: {'key0':{'value':0,'attr':{'name':'max'}},
    'key5':{'key6':'6'},'key1':{'value':{'key2':2,'key3':3},
    'attr':{'leiwen':'handsome','name':'\"email\"'}},'key4':4}
    :param custom_root:
    :param encoding: e.g'utf-8'
    :return: text xml or binary xml depending on encoding
    sample return: '<key0 name=max>0</key0><key5><key6>6</key6></key5>
    <key1 leiwen=handsome name="email"><key2>2</key2><key3>3</key3></key1><key4>4</key4>'
    '''
    returnxml = ''
    for key in data:
        if not isinstance(
                data[key], dict):  # if it is not a dictionary, we do not have to use recursive
            returnxml += f'<{key}>{data[key]}</{key}>'
        else:  # it is a dictionary, we have to use recursive
            _str_attr = ''
            # if the reason that data[key]'s type is dict data[key] has 'attr'
            # to present
            if 'attr' in data[key]:
                for attrkey in data[key]['attr']:
                    _str_attr += f' {attrkey}={data[key]["attr"][attrkey]}'
                if not isinstance(
                        data[key]["value"], dict):
                    # if value of data[key]'s type is not dict
                    returnxml += f'<{key}{_str_attr}>' \
                        f'{data[key]["value"]}</{key}>'
                else:
                # if data[key] has no attr but it is a dict,
                # sice the data[key]["value"] must be dictionary
                    returnxml += f'<{key}{_str_attr}>' \
                        f'{dicttoxmlcreator(data = data[key]["value"])}</{key}>'
            else:
                returnxml += f'<{key}{_str_attr}>{dicttoxmlcreator(data = data[key])}</{key}>'
    # replace special character
    returnxml = replace_acsii_control_characters(string=returnxml, control_characters=['\x01',
                                                                                        '\x02',
                                                                                        '\x03',
                                                                                        '\x04',
                                                                                        '\x05',
                                                                                        '\x08',
                                                                                        '\x1a',
                                                                                        '\x0b',
                                                                                        '\x0c'])

    if custom_root:
        returnxml = dicttoxmlcreator(data={custom_root: returnxml})
    if encoding:
        returnxml = f'<?xml version=\"1.0\" encoding=\"{encoding}\" ?>' + returnxml
        returnxml = returnxml.encode(encoding)

    return returnxml


def replace_acsii_control_characters(string, replace_string='', control_characters=[
    u'\x01', u'\x02', u'\x03', u'\x04', u'\x05',
    u'\x08', u'\x09', u'\x0A', u'\x0B', u'\x0C', u'\x0D']):
    """
    This function is to replace acsii control string
    :param string:
    :param replace_string:
    :param control_characters:
    :return:
    """
    if isinstance(control_characters, str):
        control_characters = [control_characters]
    for rep in control_characters:
        string = string.replace(rep, replace_string)
    return string


def get_last_date_of_last_month(date):
    """
    get the last date of Last Month of the input date
    :param date: date
    :return: Datetime
    """
    return date.replace(day=1) - timedelta(days=1)


def get_first_date_of_next_month(date):
    """
    get the first date of Next Month of the input date
    :param date: date
    :return: Datetime
    """
    return date.replace(day=calendar.monthrange(
        date.year, date.month)[1]) + timedelta(days=1)


def cartesian_products(df1, df2, key='key???????????'):
    '''
    make cartesian products for two data frame
    :param df1: pandas df
    :param df2: pandas df
    :param key: temporory join key, it is not important
    :return: cartesian productd pandas df
    '''
    df1[key] = 1
    df2[key] = 1
    return pd.merge(df1, df2, on=key).drop(columns=key)


def getalltextoftag(tag, seperator=';', empty_placer={}):
    """
    get of tags in to a string
    :param tag:
    :param seperator:
    :param empty_placer:
    :return:
    """
    string = ''
    if tag.text:
        string += tag.text
    childtags = tag.xpath('./*')
    for childtag in childtags:
        if childtag.tag in empty_placer.keys() and getalltextoftag(childtag, seperator,
        empty_placer) == '':
            # if this tag is empty at all, and if we need place with a empty holder
            string += empty_placer[childtag.tag] + seperator
        else:
            string += getalltextoftag(childtag, '', empty_placer) + seperator
    return string


def str_to_list(obj):
    """
    change a string to a list contains that string
    :param obj: object
    :return: list
    """
    if isinstance(obj, str):
        obj = [obj]
    elif obj is None:
        obj = []
    elif isinstance(obj, list):
        pass
    else:
        raise TypeError('The object is not str, nor None, nor list')
    return obj


def run_script_in_python(target_path, target_script, return_to_cwd=True):
    from pathlib import Path
    import os
    import sys
    import subprocess

    path_target_path = Path(target_path)
    path_target_main_script = Path(target_script)
    path_home = Path.cwd()  # get cwd path
    os.chdir(path_target_path)
    sys.path.append(path_target_path)  # to prevent python cannot find correct module
    exit_code = subprocess.call(f"python {path_target_main_script}", shell=True)

    if return_to_cwd:
        os.chdir(path_home)
    if str(exit_code) == '1':
        return "Failded"
    elif str(exit_code) == '0':
        return "Succeeded"

def normalize_path(path):
    return str(Path(path))

def objects_by_id(_id):
    import gc
    #import ctypes
    for obj in gc.get_objects():
        if id(obj) == _id:
            return obj
            # other way
            # return ctypes.cast(_id,ctypes.py_object)
    raise Exception("No found")

def count_weekday(target_year,target_month,multiplier=8):
    import calendar
    count_day = 0
    for day in range(1,calendar.monthrange(target_year,target_month)[1]+1):
        datetime_d = datetime(year = target_year, month = target_month, day = day)
        if datetime_d.isoweekday()<=5:
            count_day += 1
    return count_day*multiplier

def get_time_stamp(obj_datetime = None, time_format ="%Y-%m-%dT%H:%M:%S.%f",time_zone='America/New_York',drop_digit=3):
    if not obj_datetime:
        obj_datetime = datetime.now()
    if drop_digit:
        str_time = obj_datetime.strftime(time_format)[:-abs(drop_digit)]
    else:
        str_time = obj_datetime.strftime(time_format)
    if time_zone:
        import pytz
        timezone = pytz.timezone(time_zone)
        obj_datetime = timezone.localize(obj_datetime)
        str_time += obj_datetime.strftime("%z")
    return str(str_time)


def get_iso_time_stamp(obj_datetime=None, time_zone='America/New_York', timespec='milliseconds'):
    if not obj_datetime:
        obj_datetime = datetime.now()
    if time_zone:
        if isinstance(obj_datetime, pd.Timestamp):
            obj_datetime = obj_datetime.to_pydatetime()
        import pytz
        timezone = pytz.timezone(time_zone)
        obj_datetime = timezone.localize(obj_datetime)
    return obj_datetime.isoformat(timespec=timespec)

def find_month(obj_datetime,offset=-2,default_day=31):
    from calendar import monthrange
    # fix default days in the month
    if offset == 0:
        obj_datetime_target_month = obj_datetime
    elif offset <0:
        obj_datetime_target_month_plus_one =find_month(obj_datetime,offset+1,default_day)
        obj_datetime_target_month = obj_datetime_target_month_plus_one.replace(day=1)-timedelta(days=1)
    else:
        obj_datetime_target_month_minus_one =find_month(obj_datetime,offset-1,default_day)
        obj_datetime_target_month = obj_datetime_target_month_minus_one.replace(day=monthrange(year=obj_datetime_target_month_minus_one.year,month=obj_datetime_target_month_minus_one.month)[1])+timedelta(days=1)
    #fix default day
    default_day_in_target_month = min(default_day, monthrange(year=obj_datetime_target_month.year,month=obj_datetime_target_month.month)[1])
    obj_datetime_target_month = obj_datetime_target_month.replace(day=default_day_in_target_month)
    return obj_datetime_target_month

# %% Define time admin class
class TIME_ADMIN:
    def __init__(self):
        self.start()
        pass

    def start(self):
        gc.collect()
        self.start_time = time.time()
        return self.seconds_to_date(self.start_time)

    def end(self):
        gc.collect()
        self.end_time = time.time()
        return self.seconds_to_date(self.end_time)

    def final_time(self):
        gc.collect()
        return self.hh_mm_ss(self.end_time - self.start_time)

    def check_time(self):
        return self.hh_mm_ss(time.time() - self.start_time)

    def hh_mm_ss(self, seconds):
        seconds_ = int(seconds)
        hours_, seconds_ = seconds_ // 3600, seconds_ % 3600
        minutes_, seconds_ = seconds_ // 60, seconds_ % 60
        return ('{}'.format(hours_, '02d') + ":" + '{}'.format(minutes_, '02d') + ":" + '{}'.format(seconds_, '02d'))

    def seconds_to_date(self, seconds):
        return datetime.fromtimestamp(seconds).strftime("%A, %B %d, %Y %I:%M:%S")

    def get_memory(self):
        return psutil.virtual_memory()

    def process_check(self):
        print(self.check_time(), self.get_memory())

class PROCESS_ADMIN:
    '''
    PID: The PID column is the process ID you need to give to the kill command.
    PPID: PPID is the parent process ID
    PGID: PGID is the process group ID
    WINPID: WINPID column is the process ID displayed by NT's Task Manager program

    '''
    def __init__(self, pid=None):
        if not pid:
            self._pid = os.getpid()
        else:
            self._pid = pid
        self._process = psutil.Process(self._pid)

    def get_pid(self):
        """
        get process id
        """
        return self._pid

    def kill(self):
        """
        kill current process
        """
        self._process.terminate()

    def get_memory(self):
        """
        Return a namedtuple with variable fields depending on the platform, representing memory information about the process.
        """
        return self._process.memory_info()

def type_transform(variable, target_type:type, default=None):
    try:
        return target_type(variable)
    except (TypeError,ValueError):
        return default

def shell_script_creator(command, nohup=True, parameters=None, arguments=None):
    list_command = [command]
    if parameters:
        list_parameters = [key + ' ' + item for key, item in parameters.items()]
        list_command += list_parameters

    if arguments:
        list_command += arguments
    script = str.join(' ', list_command)
    if nohup:
        script = 'nohup ' + script + ' &'
    return script