'''__author__:Leiwen Lin''' 
'''__maintainer__:'Leiwen Lin''' 
'''__createtime__:2019-09-30 15:33:23.834088'''
#https://docs.python.org/2.3/lib/node304.html
import logging
from logging.handlers import RotatingFileHandler
from logging import FileHandler
import socket
from pathlib import Path
"""
logging level:
    DEBUG: Detailed information, typically of interest only when diagnosing problems.

    INFO: Confirmation that things are working as expected.

    WARNING: An indication that something unexpected happened, or indicative of some problem in the near future (e.g. ‘disk space low’). The software is still working as expected.

    ERROR: Due to a more serious problem, the software has not been able to perform some function.

    CRITICAL: A serious error, indicating that the program itself may be unable to continue running.
logging format:

"""

def get_logger(file_path,logger_name,rotation=True,level='DEBUG',fmtstr="%(asctime)s {} %(process)d L:%(levelname)s %(name)s %(message)s".format(socket.gethostname()),maxBytes=1024**2,backupCount=10):
    logging.basicConfig(level=level,format=fmtstr)
    #mkdir if the dir does not exists
    if not Path(file_path).exists():
        Path.mkdir(Path(file_path).parent,parents=True, exist_ok=True)
    if rotation:
        my_handler = RotatingFileHandler(file_path, mode='a', maxBytes=maxBytes,backupCount=backupCount, encoding=None, delay=0)
    else:
        my_handler = FileHandler(filename=Path(file_path))
    my_handler.setFormatter(logging.Formatter(fmtstr))
    logger = logging.getLogger(logger_name)
    logger.addHandler(my_handler)
    logger.setLevel(level)
    return {"logger":logger,"logger_handler":my_handler}

# def get_logger(file_path,logger_name,level='DEBUG',fmtstr="%(asctime)s {} %(process)d L:%(levelname)s %(name)s %(message)s".format(socket.gethostname())):
#     if not Path(file_path).exists():
#         Path.mkdir(Path(file_path).parent,parents=True, exist_ok=True)
#     logging.basicConfig(filename=file_path, filemode='a', format=fmtstr,level=level)    #mkdir if the dir does not exists
#     logger = logging.getLogger(logger_name)
#     return {"logger":logger}

## demo
# from random import Random
# logger = get_logger('temp5.log',logger_name=__name__,maxBytes=10*1024)['logger']
# for i in range(100):
#     logger.info('dasdsa')

