from difflib import SequenceMatcher
import numpy as np
list_missing_value = ["",np.NAN,np.nan,np.NaN,None]

def simliar(a, b, threshold=0.95):
    """
    :param a:
    :param b:
    :param threshold:
    :return: if the a and b string is % of similar is greater than threshold then return true,
    othereise
    """
    return SequenceMatcher(None, a, b).ratio() > threshold


def short_cut_compare(x, y,comprise=True):
    """
    :param x:
    :param y:
    :return: if x y have incorporate relationship, or similar enough, we turn true
    """
    flag = ((x in y) or (y in x) and comprise) or simliar(x, y)
    return flag


# compare function: 'similar comparison'(similar enough considered as the
# pass the comparison)
def similar_result(x, y, dictionary, detail=False):
    if not short_cut_compare(x, y,comprise=False):
        if x in list_missing_value:
            if not detail:
                return f"Missing Value: {dictionary[0]};\n"
            else:
                return f"Missing Value: {dictionary[0]}; [ {dictionary[1]} = {y}) ]\n"
        elif y in list_missing_value:
            if not detail:
                return f"Missing Value: {dictionary[1]};\n"
            else:
                return f"Missing Value: {dictionary[1]}; [ {dictionary[0]} = {x} ]\n"
        else:
            if not detail:
                return f"Differences in {dictionary[0]} and {dictionary[1]};\n"
            else:
                return f"{dictionary[0]} = {x}; {dictionary[1]} = {y};\n"
    else:
        return ""

# compare function: 'exact comparison' (exact the same considered as the
# pass the comparison)


def compare_result(x, y, dictionary, detail=False):
    if x != y:
        if x in list_missing_value:
            if not detail:
                return f"Missing Value: {dictionary[0]};\n"
            else:
                return f"Missing Value: {dictionary[0]}; [ {dictionary[1]} = {y}) ]\n"
        elif y in list_missing_value:
            if not detail:
                return f"Missing Value: {dictionary[1]};\n"
            else:
                return f"Missing Value: {dictionary[1]}; [ {dictionary[0]} = {x} ]\n"
        else:
            if not detail:
                return f"Differences in {dictionary[0]} and {dictionary[1]};\n"
            else:
                return f"{dictionary[0]} = {x}; {dictionary[1]} = {y};\n"
    else:
        return ""


def compare_list(x, y, dictionary, detail=False):
    '''
    :param x: list x
    :param y: list y
    :param dictionary: key: name of x's name, value: name of y's name
    :param detail: whether to show compare detail
    :return: string: result of list compare
    '''
    x = set(x)
    y = set(y)
    return_string = ''
    if x == y:
        pass
    else:
        if not detail:
            return f"Missng Element in {dictionary[0]} or {dictionary[0]};\n"
    if x.difference(y) != set():
        return_string += (
            f"Missng Element in {dictionary[1]}: {x.difference(y)};\n")
    if y.difference(x) != set():
        return_string += (
            f"Missng Element in {dictionary[0]}: {y.difference(x)};\n")
    return return_string

def beautify_discrepancy_details(discrepancy_details):
    if discrepancy_details not in list_missing_value:
        list_discrepancy = discrepancy_details.split('\n')[:-1]
        str_beautified = f'{len(list_discrepancy)} Difference(s):\n'
        for line_no, discrepancy in enumerate(list_discrepancy):
            str_beautified += f"    {line_no+1}. "+discrepancy+"\n"
    else:
        str_beautified = discrepancy_details
    return str_beautified
